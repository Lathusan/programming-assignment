package Progrmming_Assignment;

import Select_Previllage.User_Input_Select_Option;

import java.util.*;

public class Admin_Privillage {

	public void admin() {

		Scanner scanin = new Scanner(System.in);

		int adminInput;
		String enter;

		System.out.println("");
		System.out.println(" Your Privillages");
		System.out.println("==================");
		System.out.println("1. Act Cashier");
		System.out.println("2. View Summary");
		System.out.println("3. See Product list");
		System.out.println("4. Add or Remove Products");
		System.out.println("5. Create or Delete users");
		System.out.println("6. Exit");

		do {
			adminInput = User_Input_Select_Option.readNumber("Enter the number");

			if (adminInput == 1) {

				Product_Details pDetails = new Product_Details();
				pDetails.productEnter();

			} else if (adminInput == 2) {

				System.out.println("Sorry View Summary privilege is not available.");

			} else if (adminInput == 3) {

				System.out.println("See Product list is not available.");

			} else if (adminInput == 4) {

				System.out.println("Add or Remove Products is not available.");

			} else if (adminInput == 5) {

				System.out.println("Create or Delete users is not available.");

			} else if (adminInput == 6) {
				System.out.println("Do you want to exit? ");
				System.out.println(" 7:Yes ");
				System.out.println(" 8:No ");

			} else if (adminInput == 8) {

				Admin_Privillage incall = new Admin_Privillage();
				incall.admin();

			}

		} while (adminInput == 7);

		System.out.println("  !!! Thank You !!!");

	}

}
